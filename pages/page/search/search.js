
Page({
  data: {
    va:"",
    cssError:""
  },
  onLoad: function () {
        this.setData({
            cssError:""
        });
  },
  formSubmit:function(e){

    var se=e.detail.value;
    var va=se.va.trim();
    if(!!va){
        wx.navigateTo({
            url: '../searchPage/searchPage?va='+va
        });
        return
    }else{
        this.setData({
            cssError:"error"
        });
    }
  },
  bindfocus:function(){
        this.setData({
            cssError:""
        });
  }
})