
var list=require('../../../util/list');
var li=[];
var hide='';
Page({
  data: {
    va:"",
    searchList:[],
    pokeinfo:false,
    hide:""
  },
  onLoad: function (options) {
    li=[];
    var va=options.va;
    for(var i in list.rows){
      if(list.rows[i].name.indexOf(va)!=-1||list.rows[i].id.indexOf(va)!=-1){
        list.rows[i].n=i;
          li.push(list.rows[i])
      }
    }
    this.setData({    
      va: va,
      searchList:li,
      pokeinfo:!li.length
    });
  },
  kindToggles:function(){
    if(!hide){
      hide='hide';
    }else{
      hide='';
    }
    this.setData({
      hide:hide
    });
  }
})